package com.onego.entity;

import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
public class t_User {

    public t_User(String username,Integer age){
        this.username=username;
        this.age=age;
    }
    private Long id;
    @NonNull
    @Size(min=5,max=10)
    private String username ;
    private Integer age;
    private String address;
    private String memo;
}
