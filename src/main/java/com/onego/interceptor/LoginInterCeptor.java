package com.onego.interceptor;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import java.io.IOException;

@Component
public class LoginInterCeptor implements HandlerInterceptor {
    public boolean preHandler(HttpServletRequest request, HttpServletResponse response,Object object) throws IOException {
        HttpSession session=request.getSession();
        String usersession= (String) session.getAttribute("userSession");
        if (usersession==null){
            response.sendRedirect("/login");
            return false;
        }
        return true;
    }
}
