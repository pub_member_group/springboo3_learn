package com.onego;

import com.onego.peoperties.JavaStackProperties;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.cassandra.CassandraProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.system.JavaVersion;
import org.springframework.context.annotation.Bean;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@SpringBootApplication
@RequiredArgsConstructor
@EnableConfigurationProperties(value = {JavaStackProperties.class})
@RestController
@Slf4j
public class OneGoApplication {



	private final JavaStackProperties javaStackProperties;

	@Bean
	public CommandLineRunner commandLineRunner(){
		return  (args)->{
			log.info("javastack propersies:{}",javaStackProperties);
		};
	}

	@GetMapping("/")
	String home() {
		return "Spring is here!";
	}


	@GetMapping(value = "/index/{content}")
	public ModelAndView test(@PathVariable("content") String content, HttpServletRequest request){
		request.setAttribute("content",content);
		return new ModelAndView("index");
	}

	@GetMapping("/home")
	public ModelAndView home(Model model) {
		model.addAttribute("message", "Hello, Thymeleaf!");
		return new ModelAndView("home") ;
	}
	public static void main(String[] args) {
		SpringApplication.run(OneGoApplication.class,args);
		System.out.println(JavaVersion.getJavaVersion());
	}
}