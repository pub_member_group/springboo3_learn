package com.onego.controller;

import com.onego.entity.t_User;
import jakarta.validation.constraints.Size;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Random;

@RestController
@Validated
public class UserController {
    @CrossOrigin
    @GetMapping(value = "/user/json/{userid}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getJsonUserInfo(@PathVariable("userID") @Size(min=5,max = 8)String userID ){
        t_User user=new t_User("Spring boot",18);
        user.setId(Long.valueOf(userID));
        return  new ResponseEntity(user, HttpStatus.OK);
    }

    @PostMapping(value = "/user/save")
    public ResponseEntity saveUser(@RequestBody @Validated  t_User user){
        user.setId(new Random().nextLong());
        return new ResponseEntity(user,HttpStatus.OK);
    }
}
