package com.onego.controller;

import io.micrometer.common.util.StringUtils;
import jakarta.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class IndexController {
    private  final HttpSession httpSession;

    @RequestMapping(value = "login")
    @ResponseBody
    public String login(){
        return "login";
    }

    @RequestMapping("/login/submit")
    public String loginSubmit(@RequestParam("username") String username){
        if (StringUtils.isNotBlank(username)){
            httpSession.setAttribute("username",username);
            return "/index";
        }
        return "/login";
    }

    @ResponseBody
    @RequestMapping("/index")
    public String index(){
        log.info("session id :{}",httpSession.getId());
        return  "index page";
    }


    @RequestMapping("/logout")
    public String logout(){
        httpSession.invalidate();
        return "/login";
    }

}
