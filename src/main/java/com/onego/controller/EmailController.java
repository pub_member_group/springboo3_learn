package com.onego.controller;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;

@Slf4j
@RestController
@RequiredArgsConstructor
public class EmailController {
    private final JavaMailSender javaMailSender;
    private final Environment environment;

    @RequestMapping("/sendEmail")
    @ResponseBody
    public boolean sendEmail(@RequestParam("email") String email , @RequestParam("text") String text) throws MessagingException, UnsupportedEncodingException {
        MimeMessage msg=createMimeMsg(email,text,"java.png");
        javaMailSender.send(msg);
        return true;
    }


    /***
     * 创建复杂邮件
     * @param email
     * @param text
     * @param attachementClassPath
     * @return
     * @throws MessagingException
     * @throws UnsupportedEncodingException
     */
    private MimeMessage createMimeMsg(String email, String text, String attachementClassPath) throws MessagingException, UnsupportedEncodingException {
        MimeMessage msg=javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper=new MimeMessageHelper(msg,true);
        mimeMessageHelper.setFrom(environment.getProperty("mail.from"),environment.getProperty("mail.personal"));
        mimeMessageHelper.setTo(email);
        mimeMessageHelper.setBcc(environment.getProperty("mail.bcc"));
        mimeMessageHelper.setSubject(environment.getProperty("mail.subject"));
        mimeMessageHelper.setText(text);
        mimeMessageHelper.addAttachment("附件",new ClassPathResource(attachementClassPath));

        return msg;
    }

    /**
     * 简单邮件
     * @param email
     * @param text
     * @return
     */
    private SimpleMailMessage createSimpleMsg(String email,String text){
        SimpleMailMessage msg=new SimpleMailMessage();
        msg.setFrom(environment.getProperty("mail.from"));
        msg.setTo(email);
        msg.setBcc(environment.getProperty("mail.bcc"));
        msg.setSubject(environment.getProperty("mail.subject"));
        msg.setText(text);
        return msg;
    }
}
