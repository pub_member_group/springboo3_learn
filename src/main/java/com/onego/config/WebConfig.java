package com.onego.config;

import com.onego.interceptor.LoginInterCeptor;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@RequiredArgsConstructor
public class WebConfig implements WebMvcConfigurer {


    private  final LoginInterCeptor loginInterCeptor;

    public  void addInterceptors(InterceptorRegistry registry){
        registry.addInterceptor(loginInterCeptor).addPathPatterns("/**")
                .excludePathPatterns("/login/***")
                .excludePathPatterns("/static/***");
    }
}
