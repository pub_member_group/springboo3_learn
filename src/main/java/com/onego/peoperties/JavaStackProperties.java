package com.onego.peoperties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;
import java.util.Map;

@Data
@ConfigurationProperties(prefix = "onestack")
public class JavaStackProperties {
    private boolean enabled;
    private  String name;
    private  String site;
    private  String author;
    private List<String> users;
    private Map<String,String> params;
    private Security security;
}
