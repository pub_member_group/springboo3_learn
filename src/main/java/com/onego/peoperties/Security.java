package com.onego.peoperties;

import lombok.Data;

@Data
public class Security {
    private  String securityKey;
    private  String securityCode;
}
